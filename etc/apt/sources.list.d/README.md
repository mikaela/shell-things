# `/etc/apt/sources.list.d`

The `.list` files are traditional, `.sources` are modern `deb822` formatted
repository entries supported since apt `1.1` which is so old as to be
difficult to find in 2025 considering Debian Buster (oldoldstable) is on 1.8.
